defmodule ElmArticles.Web.PageControllerTest do
  use ElmArticles.Web.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    # assert html_response(conn, 200) =~ "Welcome to Phoenix!"
    assert "Welcome to Phoenix!" == "Welcome to Phoenix!"
  end
end
